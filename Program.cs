﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Kata1_PascalCase
{
    class Program
    {
        static void Main(string[] args)
        {
            string test1 = "The quick brown fox jumped over the lazy dog";
            string test2 = "The QuiCk BrOwn fox juMped Over the lAzy dog";
            string test3 = "   The      quick     brown fox jumped     over the lazy dog     ";
            string test4 = "The quick! brown fox jumped? over the.. lazy, dog";

            Console.WriteLine(PascalCaseConverter(test1));
            Console.WriteLine(PascalCaseConverter(test2));
            Console.WriteLine(PascalCaseConverter(test3));
            Console.WriteLine(PascalCaseConverter(test4));
        }

        private static string PascalCaseConverter(string sentance)
        {
            // Split every step into methods to make it readable
            string trimmed = TrimExtraSpaces(sentance);
            string noPunct = RemovePunctuation(trimmed);
            string lowercase = ConvertToLowercase(noPunct);
            string[] words = ExtractWordsFromString(lowercase);
            return JoinWordsWithCapital(words);
        }

        private static string JoinWordsWithCapital(string[] words)
        {
            // We use a string builder to join the individual words back together
            StringBuilder sb = new StringBuilder();
            foreach (string word in words)
            {
                // Capitalizes the first letter of each word and adds it to the StringBuilder
                sb.Append(char.ToUpper(word[0]) + word.Substring(1));
            }
            // Converts the StringBuilder to a string and returns it
            return sb.ToString();
        }

        private static string[] ExtractWordsFromString(string sentance)
        {
            return sentance.Split(" ");
        }

        private static string ConvertToLowercase(string sentance)
        {
            return sentance.ToLower();
        }

        private static string RemovePunctuation(string sentance)
        {
            // Regex is slow compared to other methods, 
            // look into a better performing way of doing this

            // Replaces everything that is not a letter, space, or number with nothing.
            return Regex.Replace(sentance, @"[^\w\s\d]", "");
        }

        private static string TrimExtraSpaces(string sentance)
        {
            // Regex is slow compared to other methods, 
            // look into a better performing way of doing this

            // Removes all multiple spaces and replaces with a single space.
            // Then trims any leading or trailing white spaces.
            return Regex.Replace(sentance, @"\s+", " ").Trim();
        }
    }
}
