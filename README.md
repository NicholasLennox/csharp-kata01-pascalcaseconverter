# Kata: PascalCaseConverter

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

A console application that takes a sentance and converts it to _PascalCase_. It is written in C# and is intended to be run in `Visual Studio`.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

- Clone 
- Open in `Visual Studio`
- Build application 

## Usage

- Run using `Visual Studio`

## Maintainers

[Nicholas Lennox](https://github.com/@NicholasLennox)

## Contributing



Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2021 Noroff Accelerate AS
